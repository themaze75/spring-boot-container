FROM adoptopenjdk/openjdk11:x86_64-alpine-jre-11.0.10_9
COPY ${project.build.directory}/${project.build.finalName}.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]