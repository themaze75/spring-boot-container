# Spring Boot Container

## Goals

"Hello world" type of project that simply puts up a spring-boot project and easily packages it into a container.

The basic container is simply "run the jar from a given image", but could evolve. I guess.

## Fabric8 doc

Fabric8 seems to have the docker-maven-plugin system that is the simplest, provided a little bit of familiarity with Docker and maven.

Basically, you write your docker file and can use your Maven variables.

https://dmp.fabric8.io/

```
mvn docker:build
```

will build a themaze75/boot-container-test:latest image

```
docker image inspect themaze75/boot-container-test
```

I should be able to do
```
docker run -p 8080:8080 themaze75/boot-container-test
``` 
and be OK

## Notes

```
mvn compile
mvn package
mvn test
mvn install
mvn spring-boot:run
```

* `spring-boot:run` runs your Spring Boot application.
* `spring-boot:repackage` repackages your jar/war to be executable.
* `spring-boot:start` and `spring-boot:stop` to manage the lifecycle of your Spring Boot application (i.e. for integration tests).
* `spring-boot:build-info` generates build information that can be used by the Actuator.